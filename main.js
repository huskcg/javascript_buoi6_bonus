function calcExercise1() {
  var number = document.getElementById("number").value;
  var string = "";
  for (var i = 1; i <= number; i++) {
    let flag = 0;
    for (let j = 2; j < i; j++) {
      if (i % j == 0) {
        flag = 1;
        break;
      }
    }

    if (i > 1 && flag == 0) {
      string += i + " ";
      document.getElementById("result-ex1").innerHTML = `Kết quả: ${string}`;
    }
  }
}
